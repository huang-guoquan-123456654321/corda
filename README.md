
# corda实验报告（基于区块链的隐私计算实验）

## 近期的工作
1.  blockchain与smpc理论知识的入门；
2.  用corda框架实现三个交易例子：债券、艺术品拍卖、房屋出租；
3.  阅读corda源码，笔记；

## 计划的工作
1.  目前已完成corda节点静态配置，接下来需要实现动态节点的加入，接入移动端与pc端；
2.  调研众包机制相关文献资料；
3.  设计三层contract，画流程图；

## corda网络动态加入节点
1.  Identity
2.  RPC operations
3.  Service Classes
4.  ServiceHub
5.  Compatibility zone
6.  Network certificates
7.  The network map

## 案例分析&案例测试
以案例1为例：
1.  定义IouState（IouState.java）:
![image1](https://gitee.com/huang-guoquan-123456654321/corda/raw/master/image/image1.png)

2.  定义IouContrac（IouContrac.java）:
![image1](https://gitee.com/huang-guoquan-123456654321/corda/raw/master/image/image2.png)

3.  编写IouFlow（IouIssueFlow.java 和 IouIssueFlowResponder.java）:
![image1](https://gitee.com/huang-guoquan-123456654321/corda/raw/master/image/image3.png)
![image1](https://gitee.com/huang-guoquan-123456654321/corda/raw/master/image/image4.png)
![image1](https://gitee.com/huang-guoquan-123456654321/corda/raw/master/image/image5.png)

4.  目录结构:
![image1](https://gitee.com/huang-guoquan-123456654321/corda/raw/master/image/image6.png)

5.  配置静态节点信息:
![image1](https://gitee.com/huang-guoquan-123456654321/corda/raw/master/image/image7.png)

6.  实验结果（一个公证方Notary，三个参与方A、B、C）:
启动代码：
```java
//通过打开项目根目录下的终端窗口并运行以下命令来构建节点测试网络：
gradlew.bat deployNodes
//通过运行以下命令启动节点：
build\nodes\runnodes.bat
//打开节点启动，到甲方终端（不是公证人！）运行如下命令向乙方发放99个代币：
flow start TokenIssueFlow owner: PartyB, amount: 99
//您现在可以通过在各自的终端中运行以下命令来查看甲方和乙方（但不是丙方！）的保险库中的令牌：
run vaultQuery contractStateType: bootcamp.TokenState
```
![image1](https://gitee.com/huang-guoquan-123456654321/corda/raw/master/image/image8.png)

## 源码的阅读：
1.  State api：ContractState、party
2.  Transaction api：CommandWithParties、LedgerTransaction
3.  Commands api：CommandWithParties
4.  Contract api：自定义verify方法
5.  Flow api：FlowLogic、TransactionBulider


## 参考资源
1.  corda官方文档：https://docs.r3.com/
2.  youtube教学视频：https://www.youtube.com/watch?v=f2TNd7jSL1U&list=PLi1PppB3-YrWXZEtOnp0pyLnnP2zjJCZe
3.  cnblog其他博主：

https://blog.csdn.net/li_jiachuan/category_8080009.html
https://blog.csdn.net/outsanding/category_7966126.html

