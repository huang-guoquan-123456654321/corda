package examples1;

import com.google.common.collect.ImmutableList;
import net.corda.core.contracts.ContractState;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/* Our state, defining a shared fact on the ledger.
 * See src/main/java/examples/ArtState.java for an example. */
//@BelongsToContract(IouContract.class)

//@BelongsToContract(IouContract.class)
public class IouState implements  ContractState{

    //参与者与其他信息
    private Party issur;    //发行人
    private Party owner;    //拥有人
    private int amount;     //数量

    @NotNull
    @Override
    public List<AbstractParty> getParticipants() {
        //得到交易所有的参与者
        return ImmutableList.of(issur, owner);
    }

    public IouState(Party issur, Party owner, int amount) {
        this.issur = issur;
        this.owner = owner;
        this.amount = amount;
    }

    public Party getIssur() {
        return issur;
    }

    public void setIssur(Party issur) {
        this.issur = issur;
    }

    public Party getOwner() {
        return owner;
    }

    public void setOwner(Party owner) {
        this.owner = owner;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}