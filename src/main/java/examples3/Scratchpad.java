package examples3;


import net.corda.core.contracts.StateAndRef;
import net.corda.core.identity.Party;
import net.corda.core.transactions.TransactionBuilder;

import java.security.PublicKey;

/**
 * TransactionBuilder 使用例子
 */
public class Scratchpad {
    public static void main(String[] args) {
        StateAndRef<HouseState> inputState = null;
        HouseState outputState = new HouseState(null,"92 Broadlands Road",null);
        PublicKey requiredSigner = outputState.getOwner().getOwningKey();

        /**
         * 建立TransactionBuilder
         */
        TransactionBuilder transactionBuilder = new TransactionBuilder();
        Party notary = null;

        /**
         * 假设所有的参与方为空，房屋出租例子的租方，出租方，公证方
         */
        transactionBuilder.setNotary(notary);
        transactionBuilder.addOutputState(outputState,"examples3.HouseState");
//        transactionBuilder.addCommand(new HouseContract.Register(),requiredSigner);

//        transactionBuilder.verify(serverHub);
    }

}
