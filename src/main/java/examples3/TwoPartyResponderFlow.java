package examples3;

import co.paralleluniverse.fibers.Suspendable;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.FlowSession;
import net.corda.core.flows.InitiatedBy;

/**
 * rpc接收方
 */
@InitiatedBy(TwoPartyInitiatingFlow.class)
public class TwoPartyResponderFlow extends FlowLogic<Integer> {

    private FlowSession counterPartySession;

    public TwoPartyResponderFlow(FlowSession counterPartySession) {
        this.counterPartySession = counterPartySession;
    }

    @Suspendable
    public Integer call() throws FlowException {

        //接受TwoPartyInitiatingFlow发送的信息
        int reveivedInt = counterPartySession.receive(Integer.class).unwrap(it->{
            //校验信息的合法性
           if(it>10){
               throw new IllegalArgumentException("Number is too high !");
           }
           return it;
        });
        //信息做业务逻辑处理
        int receivedIntPlusOne = reveivedInt+1;

        //信息返回给TwoPartyInitiatingFlow
        counterPartySession.send(receivedIntPlusOne);
        return null;
    }
}
