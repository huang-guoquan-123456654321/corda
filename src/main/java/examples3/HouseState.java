package examples3;

import com.google.common.collect.ImmutableList;
import net.corda.core.contracts.ContractState;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class HouseState implements ContractState {

    private Party owner;
    private String address;
    private Party resident;

    public HouseState(Party owner, String address, Party resident) {
        this.owner = owner;
        this.address = address;
        this.resident = resident;
    }

    public Party getOwner() {
        return owner;
    }

    public void setOwner(Party owner) {
        this.owner = owner;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Party getResident() {
        return resident;
    }

    public void setResident(Party resident) {
        this.resident = resident;
    }

    @NotNull
    @Override
    public List<AbstractParty> getParticipants() {
        //写法一：
        List<AbstractParty> participants = new ArrayList<>();
        participants.add(owner);
        participants.add(resident);
        return participants;
//        return ImmutableList.of(owner, resident);  //获取交易所有的参与者
    }
}
